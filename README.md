Red Hat Process Automation Manager Install Demo 
===============================================
Project to automate the installation of this product localy in a standalone configuration.


Install on your machine
-----------------------
1. [Download and unzip.](https://gitlab.com/bpmworkshop/rhpam-install-demo/-/archive/main/rhpam-install-demo-main.zip)

2. Add products to installs directory, see installs/README for details and links.

3. Run 'init.sh' or 'init.bat' file. 'init.bat' must be run with Administrative privileges. 

Login to http://localhost:8080/business-central  (u:erics / p:redhatpam1!)

Other users:

  * u:kieserver      p: kieserver1!

  * u: caseuser      p: redhatpam1!

  * u: casemanager   p: redhatpam1!

  * u: casesupplier  p: redhatpam1!

Enjoy installed and configured Red Hat Process Automation Manager.


Install in a local container (Podman)
-------------------------------------
1. [Download and unzip.](https://gitlab.com/bpmworkshop/rhpam-install-demo/-/archive/main/rhpam-install-demo-main.zip)

2. Add products to installs directory, see installs/README for details and links.

3. [Install Podman on your machine](https://podman.io/getting-started/installation), the following instructions are OSX as an example.

4. Set up the virtual machine with 6GB memory:

  ```
  $ podman machine init --memory 6144 --disk-size 20
  ```

5. Ensure you have the proper networking setup for rootless containers:

  ```
   # In file ~/.config/containers/containers.conf make sure the CONTAINERS section 
   # has the line shown here:

   [containers]
   rootless_networking = "cni"
  ```

6. Start the virtual machine:

  ```
  $ podman machine start
  ```

7. Build the Red Hat Process Automation Manager container image from the root directory of the project:

  ```
  $ podman build -t rhpam-install:7.11 .
  ```

8. Verify image has been added to the local container regsitry:

  ```
  $ podman image list

   REPOSITORY                            TAG         IMAGE ID      CREATED        SIZE
   localhost/rhpam-install               7.11        007ea4306e6e  2 minutes ago  4.05 GB
   docker.io/jbossdemocentral/developer  latest      b73501ac39b1  5 years ago    514 MB
  ```

9. Run the rhpam-install image with ports mapped for access:

  ```
  $ podman run -dt -p 8080:8080 -p 9990:9990 rhpam-install
  ```

10. Verify logs of container that it's started before logging in (find CONTAINER_ID with 'podman container list':

  ```
  $ podman logs -f [CONTAINER_ID]
  ```

Login to http://localhost:8080/business-central  (u:erics / p:redhatpam1!)

Enjoy installed and configured Red Hat Process Automation Manager.


Optional - Install Red Hat Process Automation Manager on OpenShift Container Platform
----------------------------------------------------------------------------
See the following project to install Red Hat Process Automation Manager in a container on OpenShift
Container Platform:

- [App Dev Cloud with Red Hat Process Automation Manager Install Demo](https://gitlab.com/redhatdemocentral/rhcs-rhpam-install-demo)


Supporting Articles
-------------------
- [Codeanywhere adventures - Creating your first container project (part 2)](https://www.schabell.org/2021/09/codeanywhere-adventures-creating-your-first-container-project-part2.html)

- [Beginners Guide to Installing Process Automation Tooling in a Local Container using Podman](https://www.schabell.org/2021/09/beginners-guide-to-rhpam-local-conainter-podman.html)

- [How to Install Red Hat Process Automation Manager 7.7](https://bit.ly/how-to-install-rhpam-77)

- [How to Install Red Hat Process Automation Manager 7.5 in Minutes](http://bit.ly/how-to-install-red-hat-process-auotomation-75-in-minutes)

- [AppDev in Cloud - How to put Red Hat Process Automation Manager in your Cloud](http://www.schabell.org/2018/11/appdev-in-cloud-how-to-put-red-hat-process-automation-manager-in-your-cloud.html)

- [Cloud Happiness - How to install OpenShift Container Platform with new images and templates in just minutes](http://www.schabell.org/2018/11/cloud-happiness-how-to-install-openshift-container-platform-with-new-images-templates-in-minutes.html)


Released versions
-----------------
See the tagged releases for the following versions of the product:

- v2.1 - JBoss EAP 7.3 and Red Hat Process Automation Manager 7.11 installed locally with podman container support.

- v2.0 - JBoss EAP 7.3 and Red Hat Process Automation Manager 7.11 installed locally.

- v1.9 - JBoss EAP 7.3 and Red Hat Process Automation Manager 7.10 installed locally.

- v1.8 - JBoss EAP 7.3 and Red Hat Process Automation Manager 7.9 installed locally.

- v1.7 - JBoss EAP 7.3 and Red Hat Process Automation Manager 7.8 installed locally.

- v1.6 - JBoss EAP 7.2 and Red Hat Process Automation Manager 7.7 installed locally.

- v1.5 - JBoss EAP 7.2 and Red Hat Process Automation Manager 7.6 installed locally.

- v1.4 - JBoss EAP 7.2 and Red Hat Process Automation Manager 7.5 installed locally.

- v1.3 - JBoss EAP 7.2 and Red Hat Process Automation Manager 7.4 installed locally.

- v1.2 - JBoss EAP 7.2 and Red Hat Process Automation Manager 7.3 installed locally.

- v1.1 - JBoss EAP 7.2 and Red Hat Process Automation Manager 7.2 installed locally.

- v1.0 - JBoss EAP 7.1.0 and Red Hat Process Automation Manager 7.1.0 installed locally.

![RHPAM Login](https://gitlab.com/bpmworkshop/rhpam-install-demo/raw/master/docs/demo-images/rhpam-login.png)

![RHPAM Business Central](https://gitlab.com/bpmworkshop/rhpam-install-demo/raw/master/docs/demo-images/rhpam-business-central.png)
